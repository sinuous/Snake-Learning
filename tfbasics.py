import tensorflow as tf

x1 = tf.constant(5)
x2 = tf.constant(6)

result = tf.multiply(x1,x2)

print(f'result {result}')

with tf.Session() as jeff:
    print('session result {}'.format(jeff.run(result)))

